﻿namespace osu_rc
{
    partial class Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Fade = new System.Windows.Forms.Timer(this.components);
            this.PopupText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Fade
            // 
            this.Fade.Enabled = true;
            this.Fade.Interval = 1;
            this.Fade.Tick += new System.EventHandler(this.Fade_Tick);
            // 
            // PopupText
            // 
            this.PopupText.AutoSize = true;
            this.PopupText.BackColor = System.Drawing.Color.Transparent;
            this.PopupText.Font = new System.Drawing.Font("Aller", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PopupText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.PopupText.Location = new System.Drawing.Point(12, 14);
            this.PopupText.Name = "PopupText";
            this.PopupText.Size = new System.Drawing.Size(17, 16);
            this.PopupText.TabIndex = 0;
            this.PopupText.Text = "...";
            this.PopupText.Click += new System.EventHandler(this.label1_Click);
            // 
            // Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.ClientSize = new System.Drawing.Size(230, 65);
            this.Controls.Add(this.PopupText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Popup";
            this.Opacity = 0D;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "osu!rc popup";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Blue;
            this.Load += new System.EventHandler(this.Popup_Load);
            this.Click += new System.EventHandler(this.Popup_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer Fade;
        public System.Windows.Forms.Label PopupText;
    }
}