﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace osu_rc
{
    static class Program
    {
        static Mutex YouShallNotPass = new Mutex(true, "{d00de16f-44cd-43c5-ac56-bd9d666e60bd}");

        public static FileVersionInfo osurc_ver1 = FileVersionInfo.GetVersionInfo("client.exe");
        public static string version1 = osurc_ver1.ProductVersion;

        public static FileVersionInfo osurc_ver2 = FileVersionInfo.GetVersionInfo("osu!rc.dll");
        public static string version2 = osurc_ver2.ProductVersion;

        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += Exception1;
            Application.ThreadException += Exception2;
            if (YouShallNotPass.WaitOne(TimeSpan.Zero, true))
            {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
            YouShallNotPass.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("You may not run multiple instances of this client.", "YouShallNotPassException", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private static void Exception1(object sender, UnhandledExceptionEventArgs e)
        {
            ShowException(e.ExceptionObject as Exception);
        }

        private static void Exception2(object sender, ThreadExceptionEventArgs e)
        {
            ShowException(e.Exception);
        }

        static void ShowException(Exception Ex)
        {
            MessageBox.Show(Ex.Message, Ex.TargetSite.ToString(),MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
