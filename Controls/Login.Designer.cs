﻿namespace osu_rc
{
    partial class Login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.login_btn = new System.Windows.Forms.Button();
            this.login_user = new System.Windows.Forms.TextBox();
            this.login_password = new System.Windows.Forms.TextBox();
            this.load_img = new System.Windows.Forms.PictureBox();
            this.autolog_checkbox = new System.Windows.Forms.CheckBox();
            this.passget = new System.Windows.Forms.Panel();
            this.settings_btn = new System.Windows.Forms.Button();
            this.load_animate = new System.Windows.Forms.Timer(this.components);
            this.SW = new System.Windows.Forms.PictureBox();
            this.Suwako_IdleBored = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.load_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SW)).BeginInit();
            this.SuspendLayout();
            // 
            // login_btn
            // 
            this.login_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.login_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.login_btn.FlatAppearance.BorderSize = 0;
            this.login_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.login_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.login_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login_btn.Font = new System.Drawing.Font("Exo 2.0 Semi Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.login_btn.Location = new System.Drawing.Point(243, 177);
            this.login_btn.Name = "login_btn";
            this.login_btn.Size = new System.Drawing.Size(42, 53);
            this.login_btn.TabIndex = 10;
            this.login_btn.UseVisualStyleBackColor = false;
            this.login_btn.Click += new System.EventHandler(this.LoginBtnClick);
            // 
            // login_user
            // 
            this.login_user.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.login_user.Font = new System.Drawing.Font("Exo 2.0", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login_user.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(36)))), ((int)(((byte)(63)))));
            this.login_user.Location = new System.Drawing.Point(15, 177);
            this.login_user.MaxLength = 20;
            this.login_user.Name = "login_user";
            this.login_user.Size = new System.Drawing.Size(227, 26);
            this.login_user.TabIndex = 11;
            this.login_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.login_user.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginUserKeyDown);
            // 
            // login_password
            // 
            this.login_password.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.login_password.Font = new System.Drawing.Font("Exo 2.0", 15.75F);
            this.login_password.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(36)))), ((int)(((byte)(63)))));
            this.login_password.Location = new System.Drawing.Point(15, 204);
            this.login_password.MaxLength = 8;
            this.login_password.Name = "login_password";
            this.login_password.PasswordChar = '♥';
            this.login_password.Size = new System.Drawing.Size(227, 26);
            this.login_password.TabIndex = 12;
            this.login_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.login_password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginPasswordKeyDown);
            // 
            // load_img
            // 
            this.load_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.load_img.Location = new System.Drawing.Point(-92, 160);
            this.load_img.Name = "load_img";
            this.load_img.Size = new System.Drawing.Size(94, 5);
            this.load_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.load_img.TabIndex = 15;
            this.load_img.TabStop = false;
            // 
            // autolog_checkbox
            // 
            this.autolog_checkbox.AutoSize = true;
            this.autolog_checkbox.Font = new System.Drawing.Font("Exo 2.0", 10.25F);
            this.autolog_checkbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.autolog_checkbox.Location = new System.Drawing.Point(90, 243);
            this.autolog_checkbox.Name = "autolog_checkbox";
            this.autolog_checkbox.Size = new System.Drawing.Size(121, 21);
            this.autolog_checkbox.TabIndex = 21;
            this.autolog_checkbox.Text = "Remember me";
            this.autolog_checkbox.UseVisualStyleBackColor = true;
            // 
            // passget
            // 
            this.passget.Cursor = System.Windows.Forms.Cursors.Hand;
            this.passget.Location = new System.Drawing.Point(0, 291);
            this.passget.Name = "passget";
            this.passget.Size = new System.Drawing.Size(300, 33);
            this.passget.TabIndex = 22;
            this.passget.Click += new System.EventHandler(this.GetPassClick);
            // 
            // settings_btn
            // 
            this.settings_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.settings_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.settings_btn.FlatAppearance.BorderSize = 0;
            this.settings_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.settings_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.settings_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settings_btn.Font = new System.Drawing.Font("Exo 2.0 Semi Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settings_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.settings_btn.Location = new System.Drawing.Point(261, 113);
            this.settings_btn.Name = "settings_btn";
            this.settings_btn.Size = new System.Drawing.Size(36, 31);
            this.settings_btn.TabIndex = 24;
            this.settings_btn.UseVisualStyleBackColor = false;
            this.settings_btn.Visible = false;
            this.settings_btn.Click += new System.EventHandler(this.SettingsBtnClick);
            // 
            // load_animate
            // 
            this.load_animate.Interval = 1;
            this.load_animate.Tick += new System.EventHandler(this.load_animate_Tick);
            // 
            // SW
            // 
            this.SW.Image = ((System.Drawing.Image)(resources.GetObject("SW.Image")));
            this.SW.Location = new System.Drawing.Point(-119, 19);
            this.SW.Name = "SW";
            this.SW.Size = new System.Drawing.Size(120, 152);
            this.SW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SW.TabIndex = 25;
            this.SW.TabStop = false;
            // 
            // Suwako_IdleBored
            // 
            this.Suwako_IdleBored.Enabled = true;
            this.Suwako_IdleBored.Interval = 20000;
            this.Suwako_IdleBored.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.SW);
            this.Controls.Add(this.settings_btn);
            this.Controls.Add(this.passget);
            this.Controls.Add(this.load_img);
            this.Controls.Add(this.autolog_checkbox);
            this.Controls.Add(this.login_btn);
            this.Controls.Add(this.login_password);
            this.Controls.Add(this.login_user);
            this.DoubleBuffered = true;
            this.Name = "Login";
            this.Size = new System.Drawing.Size(300, 325);
            this.Load += new System.EventHandler(this.LoginControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.load_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SW)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button login_btn;
        public System.Windows.Forms.PictureBox load_img;
        private System.Windows.Forms.Panel passget;
        public System.Windows.Forms.Button settings_btn;
        public System.Windows.Forms.TextBox login_user;
        public System.Windows.Forms.TextBox login_password;
        public System.Windows.Forms.CheckBox autolog_checkbox;
        private System.Windows.Forms.Timer load_animate;
        public System.Windows.Forms.PictureBox SW;
        public System.Windows.Forms.Timer Suwako_IdleBored;
    }
}
