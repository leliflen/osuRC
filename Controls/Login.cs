﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace osu_rc
{
    public partial class Login : UserControl
    {
        public Login()
        {
            InitializeComponent();
        }

        private void LoginBtnClick(object sender, EventArgs e)
        {
            if (login_user.Text.Length <= 2)
            {
                MessageBox.Show("Provided username is too short!", "osu!rc", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (login_password.Text.Length <= 7)
            {
                MessageBox.Show("Provided password is too short!", "osu!rc", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            Methods.BanchoConnect();
            load_animate.Start();
        }

        private void LoginControl_Load(object sender, EventArgs e)
        {
            BackgroundImage = Resources.Files.ui_login;
            settings_btn.BackgroundImage = Resources.Files.ui_button_settings;
            load_img.BackgroundImage = Resources.Files.ui_loading;
            login_btn.BackgroundImage = Resources.Files.ui_button_login;
            #region rounded

            Panel p1 = new Panel();
            Panel p2 = new Panel();
            Panel p3 = new Panel();
            Panel p4 = new Panel();
            p1.BackColor = Color.Transparent;
            p2.BackColor = Color.Transparent;
            p3.BackColor = Color.Transparent;
            p4.BackColor = Color.Transparent;
            p1.Size = new Size(1, 1);
            p2.Size = new Size(1, 1);
            p3.Size = new Size(1, 1);
            p4.Size = new Size(1, 1);
            p1.Location = login_user.Location;
            p2.Location = new Point(login_user.Location.X + login_user.Width + login_btn.Width, login_user.Location.Y);
            p3.Location = new Point(login_password.Location.X, login_btn.Location.Y + login_btn.Height - 1);
            p4.Location = new Point(login_password.Location.X + login_password.Width + login_btn.Width, login_btn.Location.Y + login_btn.Height - 1);
            Controls.Add(p1);
            Controls.Add(p2);
            Controls.Add(p3);
            Controls.Add(p4);
            p1.BringToFront();
            p2.BringToFront();
            p3.BringToFront();
            p4.BringToFront();

            #endregion rounded

            login_user.Text = Config.client_user;
            login_password.Text = Config.client_password;
            if (Config.client_autoconnect == true)
            {
                autolog_checkbox.Checked = true;
                login_btn.PerformClick();
            }
            login_user.Focus();
        }

        private void LoginUserKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                login_btn.PerformClick();
            }
        }

        private void LoginPasswordKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                login_btn.PerformClick();
            }
        }

        private void GetPassClick(object sender, EventArgs e)
        {
            Process.Start("https://osu.ppy.sh/p/irc");
        }

        private void SettingsBtnClick(object sender, EventArgs e)
        {
            Settings Settings = new Settings();
            Settings.ShowDialog();
        }

        private void load_animate_Tick(object sender, EventArgs e)
        {
            //390

            if (load_img.Location.X > 298)
            {
                load_img.Location = new Point(-92, 160);
            }
            else
            {
                load_img.Location = new Point(load_img.Location.X + 6, load_img.Location.Y);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            SW.Location = new Point(SW.Location.X + 1, SW.Location.Y);
            if (SW.Location == new Point(297, 19))
            {
                SW.Location = new Point(-129, 19);
                Suwako_IdleBored.Interval = 20000;
                load_img.Visible = true;
            }
            else
            {
                Suwako_IdleBored.Interval = 100;
                load_img.Visible = false;
            }
        }
    }
}