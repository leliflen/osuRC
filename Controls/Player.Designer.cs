﻿namespace osu_rc
{
    partial class Player
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pause = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.JukeBox = new System.Windows.Forms.ListBox();
            this.Volume = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).BeginInit();
            this.SuspendLayout();
            // 
            // pause
            // 
            this.pause.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.pause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pause.FlatAppearance.BorderSize = 0;
            this.pause.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.pause.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pause.Font = new System.Drawing.Font("Exo 2.0", 8.999999F, System.Drawing.FontStyle.Bold);
            this.pause.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.pause.Location = new System.Drawing.Point(2, 2);
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(26, 26);
            this.pause.TabIndex = 25;
            this.pause.TabStop = false;
            this.pause.Text = "▎ ▎";
            this.pause.UseVisualStyleBackColor = false;
            this.pause.Click += new System.EventHandler(this.pause_Click);
            // 
            // stop
            // 
            this.stop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.stop.FlatAppearance.BorderSize = 0;
            this.stop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.stop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stop.Font = new System.Drawing.Font("Exo 2.0", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.stop.Location = new System.Drawing.Point(2, 29);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(26, 26);
            this.stop.TabIndex = 27;
            this.stop.TabStop = false;
            this.stop.Text = "◼";
            this.stop.UseVisualStyleBackColor = false;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // JukeBox
            // 
            this.JukeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.JukeBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.JukeBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.JukeBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.JukeBox.FormattingEnabled = true;
            this.JukeBox.Location = new System.Drawing.Point(29, 2);
            this.JukeBox.Name = "JukeBox";
            this.JukeBox.Size = new System.Drawing.Size(236, 169);
            this.JukeBox.TabIndex = 29;
            this.JukeBox.SelectedIndexChanged += new System.EventHandler(this.JukeBox_SelectedIndexChanged);
            // 
            // Volume
            // 
            this.Volume.AutoSize = false;
            this.Volume.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.Volume.Location = new System.Drawing.Point(2, 56);
            this.Volume.Maximum = 100;
            this.Volume.Name = "Volume";
            this.Volume.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.Volume.Size = new System.Drawing.Size(26, 115);
            this.Volume.TabIndex = 30;
            this.Volume.TickFrequency = 10;
            this.Volume.Value = 100;
            this.Volume.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // Player
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(268, 172);
            this.Controls.Add(this.JukeBox);
            this.Controls.Add(this.pause);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.Volume);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(284, 211);
            this.Name = "Player";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "osu!rc player";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Player_FormClosing);
            this.Load += new System.EventHandler(this.Player_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Player_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button pause;
        public System.Windows.Forms.Button stop;
        private System.Windows.Forms.ListBox JukeBox;
        private System.Windows.Forms.TrackBar Volume;
    }
}