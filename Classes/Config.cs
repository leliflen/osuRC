﻿using System;
using System.IO;
using System.Collections.Generic;

namespace osu_rc
{
    public class Config
    {
        public enum Highlight
        {
            Custom,
            DefaultClient,
            DefaultOsu,
            WindowsBeep,
            SystemBeep,
            NoSound
        }

        public static List<string> Friends = new List<string>();

        public static Highlight     client_highlight;

        public static string        client_cfg = "client.cfg";
        public static int           client_width;
        public static int           client_height;
        public static int           client_posx;
        public static int           client_posy;
        public static string        client_user;
        public static string        client_password;
        public static bool          client_autoconnect;
        public static string        client_friends;
        public static bool          client_popups;
        public static bool          client_showmodes;

        public static bool          client_logs;

        public static void Parse()
        {
            if (File.Exists(client_cfg))
            {
                foreach (string line in File.ReadLines(client_cfg))
                {
                    if (line.StartsWith("USER$"))
                    {
                        string s = line.Replace("USER$", "");
                        if (s.Length <= 15)
                        {
                            client_user = s;
                        }
                    }
                    else if (line.StartsWith("AUTH$"))
                    {
                        string s = line.Replace("AUTH$", "");
                        if (s.Length == 8)
                        {
                            client_password = s;
                        }
                    }
                    else if (line.StartsWith("AUTO$"))
                    {
                        string s = line.Replace("AUTO$", "");
                        switch (s)
                        {
                            case "Yes":
                                client_autoconnect = true;
                                break;

                            case "Nah":
                                client_autoconnect = false;
                                break;
                        }
                    }
                    if (line.StartsWith("SIZE$"))
                    {
                        string s = line.Replace("SIZE$", "");
                        string[] t = s.Split('x');
                        client_width = Convert.ToInt32(t[0]);
                        client_height = Convert.ToInt32(t[1]);
                    }
                    if (line.StartsWith("POS$"))
                    {
                        string s = line.Replace("POS$", "");
                        string[] t = s.Split(',');
                        client_posx = Convert.ToInt32(t[0]);
                        client_posy = Convert.ToInt32(t[1]);
                    }
                    if (line.StartsWith("FRIENDS$"))
                    {
                        string s = line.Replace("FRIENDS$", "");
                        client_friends = s;
                        string[] t = s.Split(' ');
                        Friends.AddRange(t);
                        Friends.ToArray();
                    }
                    if (line.StartsWith("HIGHLIGHT$"))
                    {
                        string s = line.Replace("HIGHLIGHT$", "");
                        switch (s)
                        {
                            case "Custom":
                                client_highlight = Highlight.Custom;
                                break;
                            case "DefaultClient":
                                client_highlight = Highlight.DefaultClient;
                                break;
                            case "DefaultOsu":
                                client_highlight = Highlight.DefaultOsu;
                                break;
                            case "WindowsBeep":
                                client_highlight = Highlight.WindowsBeep;
                                break;
                            case "SystemBeep":
                                client_highlight = Highlight.SystemBeep;
                                break;
                            case "NoSound":
                                client_highlight = Highlight.NoSound;
                                break;
                        }
                    }
                    if (line.StartsWith("MODES$"))
                    {
                        string s = line.Replace("MODES$", "");
                        switch (s)
                        {
                            case "Yes":
                                client_showmodes = true;
                                break;
                            case "Nah":
                                client_showmodes = false;
                                break;
                        }
                    }
                    if (line.Contains("POPUPS$"))
                    {
                        string s = line.Replace("POPUPS$","");
                        switch (s)
                        {
                            case "Yes":
                                client_popups = true;
                                break;
                            case "Nah":
                                client_popups = false;
                                break;
                        }
                    }
                }
            }
        }

        public static void Save()
        {
            string[] cfgln = new string[12];
            cfgln[0] = string.Format("osu!rc config for {0} (last update {1})", Environment.UserName, DateTime.Now);
            cfgln[1] = "";
            cfgln[2] = string.Format("USER${0}", client_user);
            cfgln[3] = string.Format("AUTH${0}", client_password);
            switch (client_autoconnect)
            {
                case true:
                    cfgln[4] = "AUTO$Yes";
                    break;

                case false:
                    cfgln[4] = "AUTO$Nah";
                    break;
            }
            cfgln[5] = string.Format("SIZE${0}x{1}", client_width, client_height);
            cfgln[6] = string.Format("POS${0},{1}", client_posx, client_posy);
            cfgln[7] = string.Format("FRIENDS${0}", client_friends);
            switch (client_highlight)
            {
                case Highlight.Custom:
                    cfgln[8] = "HIGHLIGHT$Custom";
                    break;
                case Highlight.DefaultClient:
                    cfgln[8] = "HIGHLIGHT$DefaultClient";
                    break;
                case Highlight.DefaultOsu:
                    cfgln[8] = "HIGHLIGHT$DefaultOsu";
                    break;
                case Highlight.WindowsBeep:
                    cfgln[8] = "HIGHLIGHT$WindowsBeep";
                    break;
                case Highlight.SystemBeep:
                    cfgln[8] = "HIGHLIGHT$SystemBeep";
                    break;
                case Highlight.NoSound:
                    cfgln[8] = "HIGHLIGHT$NoSound";
                    break;
            }
            switch (client_showmodes)
            {
                case true:
                    cfgln[9] = "MODES$Yes";
                    break;
                case false:
                    cfgln[9] = "MODES$Nah";
                    break;
            }
            switch (client_popups)
            {
                case true:
                    cfgln[10] = "POPUPS$Yes";
                    break;
                case false:
                    cfgln[10] = "POPUPS$Nah";
                    break;
            }
            cfgln[11] = "LOGS$";
            File.WriteAllLines(client_cfg, cfgln);
        }
    }
}