﻿using System;
using System.Runtime.InteropServices;

namespace osu_rc
{
    [Flags]
    public enum Chiptune
    {
        Default = 0,
        Ramps = 2,
        Loop = 4,
        Surround = 512,
    }
    public enum Init
    {
        Default = 0
    }

    public static class Tune
    {
        private const string Dll = "tune.dll";

        static Player Player;
        public static bool On = false;

        [DllImport(Dll, EntryPoint = "BASSMOD_Init")] public static extern IntPtr Init(int device, int frequency, Init flags);
        [DllImport(Dll, EntryPoint = "BASSMOD_MusicLoad")] public static extern IntPtr Load(bool memory, string chiptunepath, int offset, int length, Chiptune flags);
        [DllImport(Dll, EntryPoint = "BASSMOD_Free")] public static extern IntPtr Unload();
        [DllImport(Dll, EntryPoint = "BASSMOD_MusicPlay")] public static extern bool PlayTune();
        [DllImport(Dll, EntryPoint = "BASSMOD_MusicStop")] public static extern bool StopTune();
        [DllImport(Dll, EntryPoint = "BASSMOD_MusicPause")] public static extern bool PauseTune();
        [DllImport(Dll, EntryPoint = "BASSMOD_SetVolume")] public static extern bool VolumeSet(int volume);
        [DllImport(Dll, EntryPoint = "BASSMOD_GetVolume")] public static extern IntPtr VolumeGet();
        [DllImport(Dll, EntryPoint = "BASSMOD_MusicGetName")] public static extern IntPtr GetTuneName();

        public static void Play(string tune)
        {
            Tune.Init(-1, 44100, osu_rc.Init.Default);
            Tune.Load(false, tune, 0, 0, Chiptune.Ramps | Chiptune.Loop);
            Tune.PlayTune();
        }

        public static void Stop()
        {
            Tune.StopTune();
            Tune.Unload();
        }

        public static string GetName()
        {
            IntPtr intPtr = Tune.GetTuneName();
            if (intPtr != IntPtr.Zero)
            {
                return Marshal.PtrToStringAnsi(intPtr);
            }
            return null;
        }

        public static void PlayerInit()
        {
            if (On == true)
            {
                Player.Focus();
            }
            else
            {
                Player = new Player();
                Player.Show();
            }   
        }
    }
}