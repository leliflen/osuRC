﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;

namespace osu_rc
{
    public partial class Player : Form
    {
        public Player()
        {
            InitializeComponent();
        }
        bool paused;


        private void Player_Load(object sender, EventArgs e)
        {
            Tune.On = true;
            Icon = Properties.Resources.osu_rc;
            if (Directory.Exists("mods"))
            {
                var files = Directory.EnumerateFiles("mods", "*.*", SearchOption.TopDirectoryOnly).Where(c => 
                c.EndsWith(".mod") || 
                c.EndsWith(".xm") || 
                c.EndsWith(".s3m") || 
                c.EndsWith(".it") || 
                c.EndsWith(".MOD") || 
                c.EndsWith(".XM") || 
                c.EndsWith(".S3M") || 
                c.EndsWith(".IT"));

                foreach (string a in files)
                {
                    JukeBox.Items.Add(Path.GetFileName(a));
                }
            }
            if (JukeBox.Items.Count == 0)
            {
                pause.Enabled = false;
                stop.Enabled = false;
                Volume.Enabled = false;
                JukeBox.Enabled = false;
                pause.BackColor = Color.FromArgb(230, 230, 230);
                stop.BackColor = Color.FromArgb(230, 230, 230);
                JukeBox.Items.Add("Put chiptunes in 'mods' folder!");
                JukeBox.Items.Add("Supported formats: XM, MOD, S3M, IT");
            }
            JukeBox.SelectedIndex = 0;
            paused = false;
        }

        private void pause_Click(object sender, EventArgs e)
        {
            if (paused == true)
            {
                Tune.Play("mods/" + JukeBox.SelectedIndex.ToString());
                paused = false;
                pause.Text = "▎ ▎";
            }
            else
            {
                Tune.PauseTune();
                paused = true;
                pause.Text = "▶";
            }
            
        }

        private void stop_Click(object sender, EventArgs e)
        {
            Tune.Stop();
            paused = true;
            pause.Text = "▶";
        }

        private void Player_FormClosing(object sender, FormClosingEventArgs e)
        {
            Tune.On = false;
            try { Tune.Stop(); } catch { }
        }

        private void JukeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pause.Text = "▎ ▎";
            paused = false;
            Tune.Stop();
            Tune.Play("mods/" + JukeBox.SelectedItem.ToString());
            if (Tune.GetName() == "")
            {
                Text = "osu!rc player | No Title";
            }
            else
            {
                Text = string.Format("osu!rc player | {0}", Tune.GetName());
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Tune.VolumeSet(Volume.Value);
        }

        private void Player_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Tune.Stop();
                this.Close();
            }
        }
    }
}
    