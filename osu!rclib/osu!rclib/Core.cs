﻿using System.IO;
namespace osu_rclib
{
    public class Core
    {
        //Fonts
        public byte[] font1 = Properties.Resources.Aller_Rg;
        public byte[] font2 = Properties.Resources.Exo2_0_Medium;
        public byte[] font3 = Properties.Resources.Exo2_0_Regular;
        public byte[] font4 = Properties.Resources.Exo2_0_SemiBold;

        //Sound
        public UnmanagedMemoryStream highlight_client = Properties.Resources.HIGHLIGHT_CLIENT;
        public UnmanagedMemoryStream highlight_osu = Properties.Resources.HIGHLIGHT_OSU;
        public UnmanagedMemoryStream error = Properties.Resources.UI_ERROR1;

        //Icon
        public System.Drawing.Icon ui_trayicon = Properties.Resources.UI_TRAYICON;

        //Image
        public System.Drawing.Image ui_login = Properties.Resources.UI_LOGIN;
        public System.Drawing.Image ui_popup = Properties.Resources.UI_POPUP;
        public System.Drawing.Image ui_error = Properties.Resources.UI_ERROR;
        public System.Drawing.Image ui_settings = Properties.Resources.UI_SETTINGS;
        public System.Drawing.Image ui_button_settings = Properties.Resources.UI_BUTTON_SETTINGS;
        public System.Drawing.Image ui_button_hltest = Properties.Resources.UI_BUTTON_HLTEST;
        public System.Drawing.Image ui_sidebar = Properties.Resources.UI_SIDEBAR;
        public System.Drawing.Image ui_avataroverlay = Properties.Resources.UI_AVATAROVERLAY;
        public System.Drawing.Image ui_defaultavatar = Properties.Resources.UI_DEFAULTAVATAR;
        public System.Drawing.Image ui_av_settings_idle = Properties.Resources.UI_AV_SETTINGS_IDLE;
        public System.Drawing.Image ui_av_settings_over = Properties.Resources.UI_AV_SETTINGS_OVER;
        public System.Drawing.Image ui_friends = Properties.Resources.UI_FRIENDS;
        public System.Drawing.Image ui_loading = Properties.Resources.UI_LOADING;
        public System.Drawing.Image ui_button_login = Properties.Resources.UI_BUTTON_LOGIN;
    }
}
