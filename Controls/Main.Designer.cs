﻿using System.Windows.Forms;
namespace osu_rc
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Fade = new System.Windows.Forms.Timer(this.components);
            this.userinputtext = new System.Windows.Forms.RichTextBox();
            this.LoggedIn = new System.Windows.Forms.Timer(this.components);
            this.LoginAnimate = new System.Windows.Forms.Timer(this.components);
            this.chatbox_msg = new System.Windows.Forms.RichTextBox();
            this.rc_tray = new System.Windows.Forms.NotifyIcon(this.components);
            this.SideBar = new System.Windows.Forms.Panel();
            this.FriendsList = new osu_rc.TransParentListBox();
            this.SideBarFriendUI = new System.Windows.Forms.PictureBox();
            this.SettingsBtn = new System.Windows.Forms.PictureBox();
            this.SideBarAvatar = new System.Windows.Forms.PictureBox();
            this.chatbox_user = new osu_rc.DisabledRichTextBox();
            this.chatbox_time = new osu_rc.DisabledRichTextBox();
            this.LoginFadein = new System.Windows.Forms.Timer(this.components);
            this.SideBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SideBarFriendUI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SideBarAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // Fade
            // 
            this.Fade.Enabled = true;
            this.Fade.Interval = 1;
            this.Fade.Tick += new System.EventHandler(this.Fade_Tick);
            // 
            // userinputtext
            // 
            this.userinputtext.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.userinputtext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(39)))));
            this.userinputtext.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userinputtext.Font = new System.Drawing.Font("Aller", 12.25F);
            this.userinputtext.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.userinputtext.Location = new System.Drawing.Point(128, 447);
            this.userinputtext.MaxLength = 1500;
            this.userinputtext.Multiline = false;
            this.userinputtext.Name = "userinputtext";
            this.userinputtext.Size = new System.Drawing.Size(716, 23);
            this.userinputtext.TabIndex = 5;
            this.userinputtext.Text = "";
            this.userinputtext.Visible = false;
            this.userinputtext.KeyDown += new System.Windows.Forms.KeyEventHandler(this.userinputtext_KeyDown);
            // 
            // LoggedIn
            // 
            this.LoggedIn.Enabled = true;
            this.LoggedIn.Interval = 1;
            this.LoggedIn.Tick += new System.EventHandler(this.LoggedIn_Tick);
            // 
            // LoginAnimate
            // 
            this.LoginAnimate.Interval = 1;
            this.LoginAnimate.Tick += new System.EventHandler(this.LoginAnimate_Tick);
            // 
            // chatbox_msg
            // 
            this.chatbox_msg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chatbox_msg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(39)))));
            this.chatbox_msg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chatbox_msg.Font = new System.Drawing.Font("Aller", 12.25F);
            this.chatbox_msg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.chatbox_msg.Location = new System.Drawing.Point(354, 0);
            this.chatbox_msg.Name = "chatbox_msg";
            this.chatbox_msg.ReadOnly = true;
            this.chatbox_msg.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal;
            this.chatbox_msg.Size = new System.Drawing.Size(489, 446);
            this.chatbox_msg.TabIndex = 13;
            this.chatbox_msg.Text = "";
            this.chatbox_msg.Visible = false;
            this.chatbox_msg.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.chatbox_msg_LinkClicked);
            // 
            // rc_tray
            // 
            this.rc_tray.Text = "osu!rc test";
            this.rc_tray.Visible = true;
            // 
            // SideBar
            // 
            this.SideBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.SideBar.Controls.Add(this.FriendsList);
            this.SideBar.Controls.Add(this.SideBarFriendUI);
            this.SideBar.Controls.Add(this.SettingsBtn);
            this.SideBar.Controls.Add(this.SideBarAvatar);
            this.SideBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.SideBar.Location = new System.Drawing.Point(0, 0);
            this.SideBar.Name = "SideBar";
            this.SideBar.Size = new System.Drawing.Size(128, 470);
            this.SideBar.TabIndex = 16;
            // 
            // FriendsList
            // 
            this.FriendsList.BackColor = System.Drawing.Color.Transparent;
            this.FriendsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FriendsList.Font = new System.Drawing.Font("Aller", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FriendsList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.FriendsList.FormattingEnabled = true;
            this.FriendsList.ItemHeight = 16;
            this.FriendsList.Location = new System.Drawing.Point(19, 111);
            this.FriendsList.Name = "FriendsList";
            this.FriendsList.Size = new System.Drawing.Size(92, 352);
            this.FriendsList.Sorted = true;
            this.FriendsList.TabIndex = 18;
            // 
            // SideBarFriendUI
            // 
            this.SideBarFriendUI.Location = new System.Drawing.Point(17, 94);
            this.SideBarFriendUI.Name = "SideBarFriendUI";
            this.SideBarFriendUI.Size = new System.Drawing.Size(94, 13);
            this.SideBarFriendUI.TabIndex = 17;
            this.SideBarFriendUI.TabStop = false;
            // 
            // SettingsBtn
            // 
            this.SettingsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SettingsBtn.Location = new System.Drawing.Point(79, 68);
            this.SettingsBtn.Name = "SettingsBtn";
            this.SettingsBtn.Size = new System.Drawing.Size(15, 15);
            this.SettingsBtn.TabIndex = 16;
            this.SettingsBtn.TabStop = false;
            this.SettingsBtn.Click += new System.EventHandler(this.SettingsBtn_Click);
            this.SettingsBtn.MouseEnter += new System.EventHandler(this.SettingsBtn_MouseEnter);
            this.SettingsBtn.MouseLeave += new System.EventHandler(this.SettingsBtn_MouseLeave);
            // 
            // SideBarAvatar
            // 
            this.SideBarAvatar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SideBarAvatar.Location = new System.Drawing.Point(28, 9);
            this.SideBarAvatar.Name = "SideBarAvatar";
            this.SideBarAvatar.Size = new System.Drawing.Size(74, 80);
            this.SideBarAvatar.TabIndex = 0;
            this.SideBarAvatar.TabStop = false;
            this.SideBarAvatar.Click += new System.EventHandler(this.SideBarAvatar_Click);
            // 
            // chatbox_user
            // 
            this.chatbox_user.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.chatbox_user.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(39)))));
            this.chatbox_user.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chatbox_user.Font = new System.Drawing.Font("Aller", 12.25F);
            this.chatbox_user.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.chatbox_user.Location = new System.Drawing.Point(201, 0);
            this.chatbox_user.Name = "chatbox_user";
            this.chatbox_user.ReadOnly = true;
            this.chatbox_user.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.chatbox_user.Size = new System.Drawing.Size(152, 446);
            this.chatbox_user.TabIndex = 10;
            this.chatbox_user.Text = "";
            this.chatbox_user.Visible = false;
            // 
            // chatbox_time
            // 
            this.chatbox_time.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.chatbox_time.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(39)))));
            this.chatbox_time.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chatbox_time.Font = new System.Drawing.Font("Aller", 12.25F);
            this.chatbox_time.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.chatbox_time.Location = new System.Drawing.Point(128, 0);
            this.chatbox_time.Name = "chatbox_time";
            this.chatbox_time.ReadOnly = true;
            this.chatbox_time.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.chatbox_time.Size = new System.Drawing.Size(73, 446);
            this.chatbox_time.TabIndex = 9;
            this.chatbox_time.Text = "";
            this.chatbox_time.Visible = false;
            this.chatbox_time.WordWrap = false;
            // 
            // LoginFadein
            // 
            this.LoginFadein.Interval = 1;
            this.LoginFadein.Tick += new System.EventHandler(this.LoginFadein_Tick);
            // 
            // Main
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(844, 470);
            this.Controls.Add(this.SideBar);
            this.Controls.Add(this.chatbox_msg);
            this.Controls.Add(this.chatbox_user);
            this.Controls.Add(this.userinputtext);
            this.Controls.Add(this.chatbox_time);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Opacity = 0D;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "osu!rc login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDown);
            this.Resize += new System.EventHandler(this.Main_Resize);
            this.SideBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SideBarFriendUI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SideBarAvatar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer Fade;
        private System.Windows.Forms.RichTextBox userinputtext;
        private System.Windows.Forms.Timer LoggedIn;
        private System.Windows.Forms.Timer LoginAnimate;
        public DisabledRichTextBox chatbox_time;
        public DisabledRichTextBox chatbox_user;
        private System.Windows.Forms.RichTextBox chatbox_msg;
        public NotifyIcon rc_tray;
        private Panel SideBar;
        public PictureBox SideBarAvatar;
        private PictureBox SettingsBtn;
        private PictureBox SideBarFriendUI;
        private TransParentListBox FriendsList;
        private Timer LoginFadein;


    }
}

