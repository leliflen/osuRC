﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Text;

namespace osu_rc
{
    public partial class Error : Form
    {
        public Error()
        {
            InitializeComponent();
        }

        private void Error_Load(object sender, EventArgs e)
        {
            BackgroundImage = Resources.Files.ui_error;
            copy_btn.Focus();
        }

        private void close_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void copy_btn_Click(object sender, EventArgs e)
        {
            StringReader errorcode = new StringReader(ErrorBox.Text);
            StringBuilder sb = new StringBuilder();
            string line;
            while (true)
            {
                line = errorcode.ReadLine();
                if (line != null)
                {
                    line = line + "\n";
                }
                else
                {
                    line = line + "\n";
                    break;
                }
                sb.AppendLine(line.ToString());
            } 
            Clipboard.SetText(Convert.ToString(sb));          
            copy_btn.Text = "Copied!";
        }

        private void Error_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                this.Close();
            }
        }
    }
}
