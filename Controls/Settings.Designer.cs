﻿namespace osu_rc
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showpopups_chk = new System.Windows.Forms.CheckBox();
            this.autologin_chk = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.HighlightSelect = new System.Windows.Forms.ComboBox();
            this.save_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.IgnoreListBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chmodes_chk = new System.Windows.Forms.CheckBox();
            this.logs_chk = new System.Windows.Forms.CheckBox();
            this.HLTest = new System.Windows.Forms.Button();
            this.ver = new System.Windows.Forms.Label();
            this.FriendsListBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // showpopups_chk
            // 
            this.showpopups_chk.AutoSize = true;
            this.showpopups_chk.BackColor = System.Drawing.Color.Transparent;
            this.showpopups_chk.Font = new System.Drawing.Font("Aller", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showpopups_chk.Location = new System.Drawing.Point(240, 56);
            this.showpopups_chk.Name = "showpopups_chk";
            this.showpopups_chk.Size = new System.Drawing.Size(163, 19);
            this.showpopups_chk.TabIndex = 0;
            this.showpopups_chk.Text = "Show notification popups";
            this.showpopups_chk.UseVisualStyleBackColor = false;
            // 
            // autologin_chk
            // 
            this.autologin_chk.AutoSize = true;
            this.autologin_chk.BackColor = System.Drawing.Color.Transparent;
            this.autologin_chk.Font = new System.Drawing.Font("Aller", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autologin_chk.Location = new System.Drawing.Point(423, 56);
            this.autologin_chk.Name = "autologin_chk";
            this.autologin_chk.Size = new System.Drawing.Size(136, 19);
            this.autologin_chk.TabIndex = 1;
            this.autologin_chk.Text = "Autologin on startup";
            this.autologin_chk.UseVisualStyleBackColor = false;
            this.autologin_chk.CheckedChanged += new System.EventHandler(this.autologin_chk_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Aller", 8.999999F);
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Highlight sound";
            // 
            // HighlightSelect
            // 
            this.HighlightSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HighlightSelect.Font = new System.Drawing.Font("Aller", 8.999999F);
            this.HighlightSelect.FormattingEnabled = true;
            this.HighlightSelect.Items.AddRange(new object[] {
            "Custom (data/highlight.wav)",
            "Default (client)",
            "Default (osu!)",
            "Windows Beep",
            "System Beep",
            "No sound"});
            this.HighlightSelect.Location = new System.Drawing.Point(12, 63);
            this.HighlightSelect.Name = "HighlightSelect";
            this.HighlightSelect.Size = new System.Drawing.Size(185, 23);
            this.HighlightSelect.TabIndex = 3;
            // 
            // save_btn
            // 
            this.save_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.save_btn.FlatAppearance.BorderSize = 0;
            this.save_btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.save_btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.save_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.save_btn.Font = new System.Drawing.Font("Exo 2.0", 11.25F);
            this.save_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.save_btn.Location = new System.Drawing.Point(12, 273);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(569, 31);
            this.save_btn.TabIndex = 11;
            this.save_btn.Text = "Save && Close";
            this.save_btn.UseVisualStyleBackColor = false;
            this.save_btn.Click += new System.EventHandler(this.SaveBtnClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Aller", 8.999999F);
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Friends list (seperated by space)";
            // 
            // IgnoreListBox
            // 
            this.IgnoreListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IgnoreListBox.Enabled = false;
            this.IgnoreListBox.Font = new System.Drawing.Font("Aller", 8.999999F);
            this.IgnoreListBox.Location = new System.Drawing.Point(12, 217);
            this.IgnoreListBox.Multiline = true;
            this.IgnoreListBox.Name = "IgnoreListBox";
            this.IgnoreListBox.Size = new System.Drawing.Size(569, 45);
            this.IgnoreListBox.TabIndex = 15;
            this.IgnoreListBox.Text = "not working yet";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Aller", 8.999999F);
            this.label3.Location = new System.Drawing.Point(12, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Ignore list (seperated by space)";
            // 
            // chmodes_chk
            // 
            this.chmodes_chk.AutoSize = true;
            this.chmodes_chk.BackColor = System.Drawing.Color.Transparent;
            this.chmodes_chk.Font = new System.Drawing.Font("Aller", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chmodes_chk.Location = new System.Drawing.Point(240, 72);
            this.chmodes_chk.Name = "chmodes_chk";
            this.chmodes_chk.Size = new System.Drawing.Size(169, 19);
            this.chmodes_chk.TabIndex = 17;
            this.chmodes_chk.Text = "Show +o/+v modes in chat";
            this.chmodes_chk.UseVisualStyleBackColor = false;
            // 
            // logs_chk
            // 
            this.logs_chk.AutoSize = true;
            this.logs_chk.BackColor = System.Drawing.Color.Transparent;
            this.logs_chk.Enabled = false;
            this.logs_chk.Font = new System.Drawing.Font("Aller", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logs_chk.Location = new System.Drawing.Point(423, 72);
            this.logs_chk.Name = "logs_chk";
            this.logs_chk.Size = new System.Drawing.Size(141, 19);
            this.logs_chk.TabIndex = 18;
            this.logs_chk.Text = "Autosave chat to logs";
            this.logs_chk.UseVisualStyleBackColor = false;
            // 
            // HLTest
            // 
            this.HLTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.HLTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.HLTest.FlatAppearance.BorderSize = 0;
            this.HLTest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.HLTest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(38)))), ((int)(((byte)(111)))));
            this.HLTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HLTest.Font = new System.Drawing.Font("Exo 2.0", 11.25F);
            this.HLTest.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.HLTest.Location = new System.Drawing.Point(203, 63);
            this.HLTest.Name = "HLTest";
            this.HLTest.Size = new System.Drawing.Size(24, 24);
            this.HLTest.TabIndex = 20;
            this.HLTest.UseVisualStyleBackColor = false;
            this.HLTest.Click += new System.EventHandler(this.HLTest_Click);
            // 
            // ver
            // 
            this.ver.AutoSize = true;
            this.ver.BackColor = System.Drawing.Color.Transparent;
            this.ver.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.ver.Location = new System.Drawing.Point(523, 9);
            this.ver.Name = "ver";
            this.ver.Size = new System.Drawing.Size(58, 13);
            this.ver.TabIndex = 21;
            this.ver.Text = "0.0.0.0000";
            this.ver.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FriendsListBox
            // 
            this.FriendsListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FriendsListBox.Font = new System.Drawing.Font("Aller", 8.999999F);
            this.FriendsListBox.Location = new System.Drawing.Point(12, 113);
            this.FriendsListBox.Multiline = true;
            this.FriendsListBox.Name = "FriendsListBox";
            this.FriendsListBox.Size = new System.Drawing.Size(569, 80);
            this.FriendsListBox.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(506, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "error test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(593, 316);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ver);
            this.Controls.Add(this.HLTest);
            this.Controls.Add(this.logs_chk);
            this.Controls.Add(this.chmodes_chk);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.IgnoreListBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FriendsListBox);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.HighlightSelect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.autologin_chk);
            this.Controls.Add(this.showpopups_chk);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "osu!rc settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Settings_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox showpopups_chk;
        private System.Windows.Forms.CheckBox autologin_chk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox HighlightSelect;
        public System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IgnoreListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chmodes_chk;
        private System.Windows.Forms.CheckBox logs_chk;
        public System.Windows.Forms.Button HLTest;
        private System.Windows.Forms.Label ver;
        private System.Windows.Forms.TextBox FriendsListBox;
        private System.Windows.Forms.Button button1;
    }
}