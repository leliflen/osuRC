﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace osu_rc
{
    public partial class Popup : Form
    {
        public Popup()
        {
            InitializeComponent();
        }

        enum PopupState
        {
            FadeIn,
            FadeOut,
            FaDead
        }

        PopupState ps;
        int open;

        private void Popup_Load(object sender, EventArgs e)
        {
            this.BackgroundImage = Resources.Files.ui_popup;
            Notification.PopupExists = true;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - this.Width, Screen.PrimaryScreen.Bounds.Height - 120);
        }

        private void Fade_Tick(object sender, EventArgs e)
        {
            if (this.Location.X != Screen.PrimaryScreen.Bounds.Width - this.Width - 10)
            {
                this.Location = new Point(this.Location.X - 1, this.Location.Y);
            }
            switch (ps)
            {
                case PopupState.FadeIn:
                    this.Opacity += 0.05;
                    break;
                case PopupState.FadeOut:
                    this.Opacity -= 0.1;
                    break;
                case PopupState.FaDead:
                    Notification.PopupExists = false;
                    this.Close();
                    break;
            }

            if (open <= 20)
            {
                ps = PopupState.FadeIn;
            }
            else if (open >= 300)
            {
                if (open > 310)
                {
                    ps = PopupState.FaDead;

                }
                else
                {
                    ps = PopupState.FadeOut;
                }
            }
            open += 1;
        }

        public void Init()
        {
            PopupText.Text = "";
            open = 20;
        }

        private void Popup_Click(object sender, EventArgs e)
        {
            open = 309;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            open = 309;
        }

    }
}
