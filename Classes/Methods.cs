﻿using System;
using System.Drawing;
using System.IO;

namespace osu_rc
{
    public class Methods
    {
        public static void BanchoConnect()
        {
            Main.LoginCtrl.settings_btn.Enabled = false;
            Main.LoginCtrl.login_user.Enabled = false;
            Main.LoginCtrl.login_password.Enabled = false;
            Main.LoginCtrl.login_btn.Enabled = false;         
            Main.LoginCtrl.autolog_checkbox.Enabled = false;

            Main.LoginCtrl.login_btn.BackColor = Color.FromArgb(230, 230, 230);
            Main.LoginCtrl.settings_btn.BackColor = Color.FromArgb(230, 230, 230);
            Main.LoginCtrl.settings_btn.BackgroundImage = null;

            if (Main.LoginCtrl.autolog_checkbox.Checked == true)
            {
                Config.client_autoconnect = true;
            }

            Config.client_user = Main.LoginCtrl.login_user.Text;
            Config.client_password = Main.LoginCtrl.login_password.Text;

            Main.osurc = new IrcClient("irc.ppy.sh", 6667);
            Main.osurc.Nick = Config.client_user;
            Main.osurc.ServerPass = Config.client_password;
            Main.osurc.Connect();
            Main.logged = true;
        }

        public static void CheckDataFolder()
        {
            if (!Directory.Exists("data"))
            {
                Directory.CreateDirectory("data");
            }
        }

    }
}
