﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace osu_rc
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            BackgroundImage = Resources.Files.ui_settings;
            HLTest.BackgroundImage = Resources.Files.ui_button_hltest;
            ver.Text = Application.ProductVersion;
            switch (Config.client_highlight)
            {
                case Config.Highlight.Custom:
                    HighlightSelect.SelectedIndex = 0;
                    break;
                case Config.Highlight.DefaultClient:
                    HighlightSelect.SelectedIndex = 1;
                    break;
                case Config.Highlight.DefaultOsu:
                    HighlightSelect.SelectedIndex = 2;
                    break;
                case Config.Highlight.WindowsBeep:
                    HighlightSelect.SelectedIndex = 3;
                    break;
                case Config.Highlight.SystemBeep:
                    HighlightSelect.SelectedIndex = 4;
                    break;
                case Config.Highlight.NoSound:
                    HighlightSelect.SelectedIndex = 5;
                    break;
            }
            switch (Config.client_autoconnect)
            {
                case true:
                    autologin_chk.Checked = true;
                    break;
                case false:
                    autologin_chk.Checked = false;
                    break;
            }
            switch (Config.client_showmodes)
            {
                case true:
                    chmodes_chk.Checked = true;
                    break;
                case false:
                    chmodes_chk.Checked = false;
                    break;
            }
            switch (Config.client_popups)
            {
                case true:
                    showpopups_chk.Checked = true;
                    break;
                case false:
                    showpopups_chk.Checked = false;
                    break;
            }
            FriendsListBox.Text = Config.client_friends;
        }

        private void Settings_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void SaveBtnClick(object sender, EventArgs e)
        {
            SaveSettings();
            this.Close();
        }

        private void SaveSettings()
        {
            Methods.CheckDataFolder();
            switch (HighlightSelect.SelectedIndex)
            {
                case 0:
                    Config.client_highlight = Config.Highlight.Custom;
                    break;
                case 1:
                    Config.client_highlight = Config.Highlight.DefaultClient;
                    break;
                case 2:
                    Config.client_highlight = Config.Highlight.DefaultOsu;
                    break;
                case 3:
                    Config.client_highlight = Config.Highlight.WindowsBeep;
                    break;
                case 4:
                    Config.client_highlight = Config.Highlight.SystemBeep;
                    break;
                case 5:
                    Config.client_highlight = Config.Highlight.NoSound;
                    break;
            }
            switch (autologin_chk.Checked)
            {
                case true:
                    Config.client_autoconnect = true;
                    break;
                case false:
                    Config.client_autoconnect = false;
                    break;
            }
            switch (chmodes_chk.Checked)
            {
                case true:
                    Config.client_showmodes = true;
                    break;
                case false:
                    Config.client_showmodes = false;
                    break;
            }
            switch (showpopups_chk.Checked)
            {
                case true:
                    Config.client_popups = true;
                    break;
                case false:
                    Config.client_popups = false;
                    break;
            }
            Config.client_friends = FriendsListBox.Text;
            Config.Save();
        }

        private void HLTest_Click(object sender, EventArgs e)
        {
            switch (HighlightSelect.SelectedIndex)
            {
                case 0:
                    try
                    {
                        System.Media.SoundPlayer hl_custom = new System.Media.SoundPlayer("data\\highlight.wav");
                        hl_custom.Play();
                        hl_custom.Dispose();
                    }
                    catch 
                    { 
                        MessageBox.Show("File not found." + Environment.NewLine+ @"Make sure you have the file under the directory 'data\highlight.wav'."); 
                    } 
                    break;
                case 1:
                    System.Media.SoundPlayer hl_client = new System.Media.SoundPlayer();
                    hl_client.Stream = Resources.Files.highlight_client;
                    hl_client.Stream.Position = 0;
                    hl_client.Play();
                    hl_client.Dispose();
                    break;
                case 2:
                    System.Media.SoundPlayer hl_osu = new System.Media.SoundPlayer();
                    hl_osu.Stream = Resources.Files.highlight_osu;
                    hl_osu.Stream.Position = 0;
                    hl_osu.Play();
                    hl_osu.Dispose();
                    break;
                case 3:
                    System.Media.SystemSounds.Beep.Play();
                    break;
                case 4:
                    Console.Beep();
                    break;
                case 5:
                    MessageBox.Show("Meow.");
                    break;
            }
        }

        private void autologin_chk_CheckedChanged(object sender, EventArgs e)
        {
            if (Main.LoginCtrl.IsDisposed == false)
            {
                Main.LoginCtrl.autolog_checkbox.CheckState = autologin_chk.CheckState;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            throw new Exception("ExceptionTest");
        }

    }
}
