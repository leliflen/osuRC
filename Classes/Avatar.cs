﻿using System;
using System.IO;
using System.Drawing;

namespace osu_rc
{
    public class Avatar  
    {
        public static Bitmap Set()
        {
            Bitmap avatar = new Bitmap(74, 80);
            Graphics graphics = Graphics.FromImage(avatar);
            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            if (File.Exists("data//avatar"))
            {
                using (FileStream afs = new FileStream("data//avatar", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    Image loaded = new Bitmap(afs);
                    graphics.DrawImage(loaded, 0, 0, 74, 74);
                }
            }
            else
            {
                graphics.DrawImage(Resources.Files.ui_defaultavatar, 0, 0, avatar.Width, avatar.Height);
            }
            graphics.DrawImage(Resources.Files.ui_avataroverlay, 0, 0, avatar.Width, avatar.Height);
            return avatar;
        }
    }
}
