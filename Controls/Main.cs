﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;

namespace osu_rc
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        public static bool logged = false;
        public static IrcClient osurc;
        public static Login LoginCtrl = new Login();

        private void Form1_Load(object sender, EventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += Exception1;
            Application.ThreadException += Exception2;
            Config.Parse();
            SideBarAvatar.Image = Avatar.Set();
            rc_tray.Icon = Resources.Files.ui_trayicon;
            SideBar.BackgroundImage = Resources.Files.ui_sidebar;
            SideBarFriendUI.BackgroundImage = Resources.Files.ui_friends;
            SettingsBtn.Image = Resources.Files.ui_av_settings_idle;
            if (Config.client_autoconnect == true) { Hide(); Fade.Stop(); ShowInTaskbar = false; }         
            Size = new Size(306, 354);
            Location = new Point(Screen.PrimaryScreen.Bounds.Width / 2 - Width / 2, Screen.PrimaryScreen.Bounds.Height / 2 - Height / 2);

            chatbox_time.SelectionColor = Color.FromArgb(234, 110, 0);
            chatbox_user.SelectionColor = Color.White;
            chatbox_msg.SelectionColor = Color.FromArgb(220, 220, 220);
            chatbox_user.SelectionAlignment = HorizontalAlignment.Right;

            Icon = Properties.Resources.osu_rc;
            LoginCtrl.Location = new Point(0, 0);
            Controls.Add(LoginCtrl);
            LoginCtrl.BringToFront();
        }

        public void Listen()
        {
            osurc.PrivateMessage += (u, m) =>
            {
                chatbox_msg.AppendText(m + "\n");
                chatbox_time.AppendText("PM\n");
                chatbox_user.AppendText(u + "\n");
            };
            osurc.ChannelMessage += (c, u, m) =>
            {
                chatbox_time.SelectionColor = Color.FromArgb(234, 110, 0);
                chatbox_user.SelectionColor = Color.White;
                chatbox_msg.SelectionColor = Color.FromArgb(220, 220, 220);
                if (m.StartsWith("ACTION"))
                {
                    if (u == osurc.Nick)
                    {
                    chatbox_time.SelectionColor = Color.FromArgb(234, 110, 0);
                    chatbox_user.SelectionColor = Color.FromArgb(225, 227, 176);
                    chatbox_msg.SelectionColor = Color.FromArgb(225, 227, 176);
                    }
                    string ac = m.Replace("ACTION", "");
                    //chatbox_msg.AppendText(String.Format("{0} *{1}{2}\n", DateTime.Now.ToLongTimeString(), u, ac));
                    chatbox_user.AppendText(String.Format("*{0}\n", u));
                    chatbox_time.AppendText(DateTime.Now.ToLongTimeString() + "\n");
                    chatbox_msg.AppendText(ac + "\n");
                }
                else if (m.ToLower().Contains(osurc.Nick.ToLower()))
                {
                    if (u != osurc.Nick)
                    {
                        if (this.ContainsFocus == false)
                        {
                            switch (Config.client_highlight)
                            {
                                case Config.Highlight.Custom:
                                    try
                                    {
                                        System.Media.SoundPlayer hl_custom = new System.Media.SoundPlayer("data\\highlight.wav");
                                        hl_custom.Play();
                                    }
                                    catch { } //file not found
                                    break;
                                case Config.Highlight.DefaultClient:
                                    System.Media.SoundPlayer hl_client = new System.Media.SoundPlayer();
                                    hl_client.Stream = Resources.Files.highlight_client;
                                    hl_client.Stream.Position = 0;
                                    hl_client.Play();
                                    break;
                                case Config.Highlight.DefaultOsu:
                                    System.Media.SoundPlayer hl_osu = new System.Media.SoundPlayer();
                                    hl_osu.Stream = Resources.Files.highlight_osu;
                                    hl_osu.Stream.Position = 0;
                                    hl_osu.Play();
                                    break;
                                case Config.Highlight.WindowsBeep:
                                    System.Media.SystemSounds.Beep.Play();
                                    break;
                                case Config.Highlight.SystemBeep:
                                    Console.Beep();
                                    break;
                                case Config.Highlight.NoSound:
                                    //lets pretend like this never happened. mmkay?
                                    break;
                            }
                            if (Config.client_popups == true)
                            {
                                Notification.Show(string.Format("Your name was mentioned\nby '{0}'!", u));
                            }
                        }
                    }
                    chatbox_time.SelectionColor = Color.FromArgb(68, 183, 51);
                    chatbox_user.SelectionColor = Color.FromArgb(68, 183, 51);
                    chatbox_msg.SelectionColor = Color.FromArgb(220, 220, 220);
                    chatbox_time.AppendText(DateTime.Now.ToLongTimeString() + "\n");
                    chatbox_user.AppendText(u + "\n");
                    chatbox_msg.AppendText(m + "\n");
                }
                else if (u == osurc.Nick)
                {
                    chatbox_time.SelectionColor = Color.FromArgb(234, 110, 0);
                    chatbox_user.SelectionColor = Color.FromArgb(225, 227, 176);
                    chatbox_msg.SelectionColor = Color.FromArgb(225, 227, 176);
                    chatbox_time.AppendText(DateTime.Now.ToLongTimeString() + "\n");
                    chatbox_user.AppendText(u + "\n");
                    chatbox_msg.AppendText(m + "\n");
                }
                else
                {
                    //chatbox_msg.SelectionColor = Color.FromArgb(234, 110, 0);
                    //chatbox_msg.AppendText(DateTime.Now.ToLongTimeString() + " ");
                    chatbox_time.AppendText(DateTime.Now.ToLongTimeString() + "\n");
                    chatbox_user.AppendText(u + "\n");
                    chatbox_msg.AppendText(m + "\n");
                    //chatbox_msg.SelectionColor = Color.White;
                    //chatbox_msg.AppendText(u + ": ");
                    //chatbox_msg.SelectionColor = Color.FromArgb(220, 220, 220);
                    //chatbox_msg.AppendText(m + "\n");
                    ///chatbox.AppendText(String.Format("{0} {1}: {2}\n", DateTime.Now.ToLongTimeString(), u, m));
                }
                chatbox_msg.ScrollToCaret();
                chatbox_time.ScrollToCaret();
                chatbox_user.ScrollToCaret();
            };
            osurc.ServerMessage += (m) =>
            {
                if (m.Contains("End of /NAMES list."))
                {
                    chatbox_msg.Clear();
                    LoginAnimate.Start();
                    chatbox_time.SelectionColor = Color.FromArgb(234, 110, 0);
                    chatbox_user.SelectionColor = Color.White;
                    chatbox_msg.SelectionColor = Color.FromArgb(220, 220, 220);
                    Notification.Show("Test popup\nWelcome to osu!Bancho.");
                    //chatbox_msg.SelectionColor = Color.FromArgb(230,230,230);
                }
                else if (m.StartsWith("+v"))
                {
                    if (Config.client_showmodes == true)
                    {
                        //string vo;
                        //vo = m.Replace("+v ", "");
                        //chatbox.AppendText(String.Format("Voice status set on {0}\n", vo));
                    }
                }
                else if (m.StartsWith("+o"))
                {
                    if (Config.client_showmodes == true)
                    {
                        //string op;
                        //op = m.Replace("+o ", "");
                        //chatbox.AppendText(String.Format("Channel operator status set on {0}\n", op));
                    }
                }
                else
                {
                    chatbox_msg.Clear();
                    chatbox_msg.ScrollToCaret();
                }

            };
            osurc.UpdateUsers += (c, u) =>
            {
                foreach (string a in u)
                { 
                    foreach (string x in Config.Friends)
                    {
                        if (a.StartsWith("@"))
                        {
                            string b = a.Replace("@", "");
                            if (x.Equals(b))
                            {
                                FriendsList.Items.Add(b);
                                CheckDuplicates();
                            }
                        }
                        if (a.StartsWith("+"))
                        {
                            string b = a.Replace("+", "");
                            if (x.Equals(b))
                            {
                                FriendsList.Items.Add(b);
                                CheckDuplicates();
                            }
                        }
                        if (x.Equals(a))
                        {
                            FriendsList.Items.Add(a);
                            CheckDuplicates();
                        }
                    }
                }
            };
            osurc.ExceptionThrown += (ex) =>
            {
                MessageBox.Show(ex.Message);
            };
            osurc.OnConnect += () =>
            {
                chatbox_msg.Clear();
                osurc.JoinChannel("#osu");
            };
            osurc.UserLeft += (c, u) =>
            {
                //
            };
            osurc.UserJoined += (c, u) =>
            {
                //
            };
        }

        private void userinputtext_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (userinputtext.Text.StartsWith("/me "))
                {
                    chatbox_time.SelectionColor = Color.FromArgb(234, 110, 0);
                    chatbox_user.SelectionColor = Color.FromArgb(225, 227, 176);
                    chatbox_msg.SelectionColor = Color.FromArgb(225, 227, 176);
                    string action = userinputtext.Text.Replace("/me ", "");

                    chatbox_user.AppendText(String.Format("*{0}\n", osurc.Nick));
                    chatbox_time.AppendText(DateTime.Now.ToLongTimeString() + "\n");
                    chatbox_msg.AppendText(action + "\n");
                    osurc.SendMessage("#osu", Convert.ToChar(1) + "ACTION " + action);
                }
                else
                {
                    chatbox_time.SelectionColor = Color.FromArgb(234, 110, 0);
                    chatbox_user.SelectionColor = Color.FromArgb(225, 227, 176);
                    chatbox_msg.SelectionColor = Color.FromArgb(225, 227, 176);

                    chatbox_time.AppendText(DateTime.Now.ToLongTimeString() + "\n");
                    chatbox_user.AppendText(osurc.Nick + "\n");
                    chatbox_msg.AppendText(userinputtext.Text + "\n");

                    osurc.SendMessage("#osu", userinputtext.Text);
                }
                userinputtext.Clear();
                userinputtext.Focus();
                chatbox_msg.ScrollToCaret();
                chatbox_time.ScrollToCaret();
                chatbox_user.ScrollToCaret();
            }
        }

        #region "misc"

        private void CheckDuplicates()
        {

            int r = FriendsList.Items.Count;
            ArrayList newList = new ArrayList();

            //Load all Items into temp array
            string[] temp = new string[r];
            for (int i = 0; i < r; i++)
            {
                temp[i] = FriendsList.Items[i].ToString();
            }

            //Add unique items to new ArrayList
            foreach (string ts in temp)
            {
                if (!newList.Contains(ts))
                {
                    newList.Add(ts);
                }
            }

            FriendsList.Items.Clear();
            foreach (string ns in newList)
            {
                FriendsList.Items.Add(ns.ToString());
            }

        }

        private void LoggedIn_Tick(object sender, EventArgs e)
        {
            if (logged == true)
            {
                LoginCtrl.load_img.Visible = true;
                Listen();
                LoggedIn.Stop();
            }
        }

        private void LoginAnimate_Tick(object sender, EventArgs e)
        {
            if (Opacity != 0)
            {
                Opacity -= 0.1;
            }
            else
            {
                LoginCtrl.Suwako_IdleBored.Enabled = false;
                LoginCtrl.SW.Dispose();
                chatbox_msg.Visible = true;
                chatbox_time.Visible = true;
                chatbox_user.Visible = true;
                LoginCtrl.Dispose();
                FormBorderStyle = FormBorderStyle.Sizable;
                Location = new Point(Config.client_posx, Config.client_posy);
                Width = Config.client_width;
                Height = Config.client_height;
                MaximumSize = new Size(0, 0);
                MinimumSize = new Size(640, 460);
                MinimizeBox = true;
                MaximizeBox = true;
                userinputtext.Visible = true;
                userinputtext.Focus();
                ShowInTaskbar = true;
                this.Text = string.Format("osu!rc | {0}", osurc.Nick);
                LoginAnimate.Stop();
                LoginFadein.Start();
            }
        }

        private void LoginFadein_Tick(object sender, EventArgs e)
        {
            if (Opacity != 1)
            {
                Opacity += 0.1;
            }
            else
            {
                LoginFadein.Stop();
            }
        }

        private void chatbox_msg_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }
        private void Main_Resize(object sender, EventArgs e)
        {
            //task managers' never ending bypass
            if (LoginCtrl.IsDisposed == false)
            {
                if (WindowState == FormWindowState.Maximized)
                {
                    WindowState = FormWindowState.Normal;
                    MessageBox.Show("The task manager, it speaks to me, it tells me: YOU SHALL NOT PASS.", "YouShallNotPassException", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Fade_Tick(object sender, EventArgs e)
        {
            if (Opacity != 1)
            {
                Opacity += 0.1;
            }
            else { Fade.Stop(); }
        }

        private static void Exception1(object sender, UnhandledExceptionEventArgs e)
        {
            ShowException(e.ExceptionObject as Exception);
        }

        private static void Exception2(object sender, ThreadExceptionEventArgs e)
        {
            ShowException(e.Exception);
        }

        static void ShowException(Exception Ex)
        {
            System.Media.SoundPlayer error = new System.Media.SoundPlayer();
            error.Stream = Resources.Files.error;
            error.Stream.Position = 0;
            error.Play();
            Error Error = new Error();
            Error.ErrorBox.SelectionColor = Color.Orange;
            Error.ErrorBox.AppendText(Ex.Message + "\n");
            Error.ErrorBox.SelectionColor = Color.FromArgb(230, 230, 230);
            Error.ErrorBox.AppendText(string.Format("{0}\n\n" + "Client version: {1}\nResources version: {2}", Ex.StackTrace, Program.version1, Program.version2));       
            Error.ShowDialog();
        }

        private void SideBarAvatar_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog a = new OpenFileDialog())
            {
                a.Title = "Select your avatar";
                a.Filter = "Images|*.PNG;*.JPG;*.BMP";
                if (a.ShowDialog() == DialogResult.OK)
                {
                    Methods.CheckDataFolder();
                    File.Copy(a.FileName, "data\\avatar", true);
                    SideBarAvatar.Image = Avatar.Set();
                }
            }

        }

        private void SettingsBtn_MouseEnter(object sender, EventArgs e)
        {
            SettingsBtn.Image = Resources.Files.ui_av_settings_over;
        }

        private void SettingsBtn_MouseLeave(object sender, EventArgs e)
        {
            SettingsBtn.Image = Resources.Files.ui_av_settings_idle;
        }

        private void SettingsBtn_Click(object sender, EventArgs e)
        {
            Settings Settings = new Settings();
            Settings.ShowDialog();
        }

        #endregion "misc"

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (LoginCtrl.IsDisposed == true)
            {
                Config.client_width = Width;
                Config.client_height = Height;
                Config.client_posx = Location.X;
                Config.client_posy = Location.Y;
                Config.Save();
                osurc.Disconnect();
            }
            else
            {
                //no change in resolution, declaring ints to avoid "assignment made to same var"
                int a = Config.client_width;
                int b = Config.client_height;
                Config.client_width = a;
                Config.client_height = b;
                Config.client_user = LoginCtrl.login_user.Text;
                Config.client_password = LoginCtrl.login_password.Text;
                Config.Save();
            }
            try { Tune.Stop();} catch { }
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                Application.Exit(); 
            }
            if (LoginCtrl.IsDisposed == true)
            {
                if (e.Control && e.Shift && e.KeyCode == Keys.P)
                {
                    Tune.PlayerInit();             
                }
           }
        }

    }
}