﻿using System;

namespace osu_rc
{
    public class Notification
    {
        static Popup Popup;
        public static bool PopupExists = false;

        public static void Show(string Text)
        {
            if (PopupExists == true)
            {
                Popup.Init();
                Popup.Focus();
                Popup.PopupText.Text = Text;
            }
            else
            {
                Popup = new Popup();
                Popup.Show();
                Popup.PopupText.Text = Text;
            }
        }
    }
}
