﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace osu_rc
{
   public class TransParentListBox : ListBox
    {
       
        public TransParentListBox()
        {
            // true???  
            // false? Paint ? Control ?  
            this.SetStyle(ControlStyles.UserPaint, true);


            // true? alpha  255  BackColor ?  
            // UserPaint  true  Control ?  
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            this.Invalidate();
            base.OnSelectedIndexChanged(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (this.Focused && this.SelectedItem != null)
            {
                //  
                Rectangle itemRect = this.GetItemRectangle(this.SelectedIndex);
                //e.Graphics.FillRectangle(Brushes.Red, itemRect);  
                e.Graphics.FillRectangle(Brushes.DeepPink, itemRect);
            }
            for (int i = 0; i < Items.Count; i++)
            {
                //  
                StringFormat strFmt = new System.Drawing.StringFormat();
                strFmt.Alignment = StringAlignment.Near; //  
                strFmt.LineAlignment = StringAlignment.Near; //  
                e.Graphics.DrawString(this.GetItemText(this.Items[i]), new Font("Aller", 8), new SolidBrush(this.ForeColor), this.GetItemRectangle(i), strFmt);

                //e.Graphics.DrawString(this.GetItemText(this.Items[i]), this.Font, new SolidBrush(this.ForeColor), this.GetItemRectangle(i));  
            }
            base.OnPaint(e);
        }  
    }
}
